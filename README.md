# Test Case Itau

O projeto foi desenvolvido usando as seguintes tecnologias

- Java 8
- Spring
- AWS (Ec2 - Dynamo - S3)
- Swagger OpenApi

## Importante

- A aplicação esta sendo executada em uma instancia Ec2, que eventualmente pode estar desligada
