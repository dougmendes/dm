package br.com.dm.documento;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;

import br.com.dm.exception.UnprocessableException;
import br.com.dm.factories.DependencyFactory;
import br.com.dm.util.ExceptionUtil;

@Component
public class DocumentoImpl implements Documento {

	private static final String S3_NAME = "dmdm";
	private static final String ERRO_AO_FAZER_UPLOAD_ARQUIVO = "erro ao fazer upload arquivo";
	private static final String ERRO_AO_FAZER_DOWNLOAD_ARQUIVO = "erro ao fazer download ou arquivo nao existe";
	private DependencyFactory dependencyFactory;
	Logger logger = LoggerFactory.getLogger(DocumentoImpl.class);
	
	@Autowired
	public DocumentoImpl(DependencyFactory dependencyFactory) {
		this.dependencyFactory=dependencyFactory;
	}
	
	@Override
	public void upload(MultipartFile doc, String cpf) {
		
		ObjectMetadata obj = new ObjectMetadata();
		
		obj.setContentType(doc.getContentType());
		
		obj.setContentLength(doc.getSize());
		
		try {
			configureS3().putObject(S3_NAME, cpf, doc.getInputStream(), obj);
		} catch (Exception e1) {
			logger.error(e1.getMessage());
			throw new UnprocessableException(ExceptionUtil.erro(ERRO_AO_FAZER_UPLOAD_ARQUIVO));
		}
		
	}
	
	@Override
	public InputStreamResource dowload(String cpf) {
		
		try {
			return new InputStreamResource(configureS3().getObject(S3_NAME, cpf).getObjectContent());
		} catch (Exception e1) {
			logger.error(e1.getMessage());
			throw new UnprocessableException(ExceptionUtil.erro(ERRO_AO_FAZER_DOWNLOAD_ARQUIVO));
		}
		
	}

	private AmazonS3 configureS3() {
		AmazonS3 s3client = AmazonS3ClientBuilder
				  .standard()
				  .withCredentials(new AWSStaticCredentialsProvider(configureCredentials()))
				  .withRegion(Regions.US_EAST_1)
				  .build();
		return s3client;
	}

	private AWSCredentials configureCredentials() {
		AWSCredentials credentials = new BasicAWSCredentials(
				dependencyFactory.getAccesKey(), 
				dependencyFactory.getSecretKey()
				);
		return credentials;
	}

}