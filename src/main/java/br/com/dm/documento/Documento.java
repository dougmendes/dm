package br.com.dm.documento;

import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public interface Documento {

	void upload(MultipartFile doc, String cpf);
	
	InputStreamResource dowload(String cpf);
	
}