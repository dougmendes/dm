package br.com.dm.factories;

import org.springframework.stereotype.Component;

@Component
public interface DependencyFactory {

	String getAccesKey();

	String getSecretKey();
	
	int getJwtExpiration();
	
	String getJwtSecret();

}