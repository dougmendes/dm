package br.com.dm.factories;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class DependencyFactoryImpl implements DependencyFactory {

	@Value(value = "${ACSSES_KEY}")
	private String acssesKey;

	@Value(value = "${SECRET_KEY}")
	private String secretKey;
	
	@Value(value = "${jwt.expiration}")
	private int expiration;
	
	@Value(value = "${jwt.secret}")
	private String secret;

	@Override
	public String getAccesKey() {
		return acssesKey;
	}

	@Override
	public String getSecretKey() {
		return secretKey;
	}

	@Override
	public int getJwtExpiration() {
		return expiration;
	}

	@Override
	public String getJwtSecret() {
		return secret;
	}
}