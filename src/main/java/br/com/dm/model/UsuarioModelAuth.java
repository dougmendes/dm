package br.com.dm.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class UsuarioModelAuth {

	private String userName;
	private String password;

}