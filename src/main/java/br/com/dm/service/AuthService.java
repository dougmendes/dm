package br.com.dm.service;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import br.com.dm.model.TokenModel;
import br.com.dm.model.UsuarioModelAuth;
import br.com.dm.security.JwtUser;
import br.com.dm.util.JwtTokenUtil;

@Service
public class AuthService {
	
	private AuthenticationManager authenticationManager;
	
	private JwtTokenUtil jwtTokenUtil;
	
	@Autowired
	private AuthService(AuthenticationManager authenticationManager, JwtTokenUtil jwtTokenUtil) {
		this.authenticationManager=authenticationManager;
		this.jwtTokenUtil=jwtTokenUtil;
	}

	public TokenModel autenticaUsuario(String authorization) {
		
		UsuarioModelAuth usuario = decodeDados(authorization);
		
		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(usuario.getUserName(), usuario.getPassword())
		);
		
		return geraToken((JwtUser)authentication.getPrincipal());
		
	}

	private TokenModel geraToken(JwtUser jwtUser) {
		
		TokenModel token = new TokenModel();
		
		token.setToken(jwtTokenUtil.generateToken(jwtUser));
		
		return token;
	}

	private UsuarioModelAuth decodeDados(String authorization) {
		
		byte[] decode = Base64.getDecoder().decode(authorization.substring("Basic".length()).trim());
		
		String credenciais = new String(decode, StandardCharsets.UTF_8);
		
		return UsuarioModelAuth.builder()
				.userName(credenciais.substring(0, credenciais.indexOf(":")))
				.password(credenciais.substring(credenciais.indexOf(":")+1, credenciais.length()))
				.build();
		
	}

}