package br.com.dm.service;

import java.text.MessageFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import br.com.dm.documento.Documento;
import br.com.dm.model.UsuarioModel;

@Service
public class DocumentoService {

	private static final String ATTACHMENT_FILENAME = "attachment; filename=";

	private static final String VAZIO = " ";
	
	private UsuarioService usuarioService;
	
	private Documento documento;
	
	@Autowired
	public DocumentoService(UsuarioService usuarioService, Documento documento) {
		this.usuarioService=usuarioService;
		this.documento=documento;
	}

	public void upload(MultipartFile doc, String cpf) {
		
		documento.upload(doc, cpf);
		
		atualizaDadosDocumentoUsuario(doc, cpf);
		
	}
	
	private void atualizaDadosDocumentoUsuario(MultipartFile documento, String cpf) {
		
		UsuarioModel usuario = new UsuarioModel();
		
		usuario.setCpf(cpf);
		
		usuarioService.cadastrarUsuario(usuario);
		
	}

	public ResponseEntity<Resource> download(String cpf) {
		
		HttpHeaders httpHeaders = new HttpHeaders();
		
		httpHeaders.setContentType(MediaType.valueOf(MediaType.APPLICATION_OCTET_STREAM_VALUE));
		
		InputStreamResource dowload = (InputStreamResource)documento.dowload(cpf);
		
		httpHeaders.set(HttpHeaders.CONTENT_DISPOSITION, MessageFormat.format(ATTACHMENT_FILENAME, dowload.getFilename()));
		
		return new ResponseEntity<Resource>(dowload, httpHeaders, HttpStatus.OK);
		
	}
	
}