package br.com.dm.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.dm.domain.Usuario;
import br.com.dm.exception.UnprocessableException;
import br.com.dm.mapper.UsuarioMapper;
import br.com.dm.model.UsuarioModel;
import br.com.dm.repository.UsuarioRepository;
import br.com.dm.util.ExceptionUtil;

@Service
public class UsuarioService {

	private static final String USUARIO_NAO_ENCONTRADO = "USUARIO NAO ENCONTRADO";
	private UsuarioRepository usuarioRepository;
	private UsuarioMapper mapper = Mappers.getMapper(UsuarioMapper.class);
	
	@Autowired
	public UsuarioService(UsuarioRepository usuarioRepository) {
		this.usuarioRepository=usuarioRepository;
	}
	
	public UsuarioModel recuperarUsuario(String cpf) {
		
		Usuario usuario = usuarioRepository.recuperarUsuario(cpf);
		
		if (null == usuario) {
			throw new UnprocessableException(ExceptionUtil.erro(USUARIO_NAO_ENCONTRADO));
		}
		
		usuario.setSenha(StringUtils.EMPTY);
		
		return mapper.converterUsuario(usuario);
		
	}
	
	public void cadastrarUsuario(UsuarioModel dadosUsuarios) {
		encodeSenha(dadosUsuarios);
		usuarioRepository.cadastrarUsuario(mapper.converterUsuario(dadosUsuarios));
	}
	
	private void encodeSenha(UsuarioModel dadosUsuarios) {
		if (StringUtils.isNotBlank(dadosUsuarios.getSenha())) {
			dadosUsuarios.setSenha(new BCryptPasswordEncoder().encode(dadosUsuarios.getSenha()));
		}
	}

	public void deletarUsuario(String cpf) {
		usuarioRepository.deletar(cpf);
	}
	
	public void alterar(UsuarioModel dadosUsuarios) {
		encodeSenha(dadosUsuarios);
		usuarioRepository.cadastrarUsuario(mapper.converterUsuario(dadosUsuarios));
	}

	public List<UsuarioModel> recuperaTodos() {
		
		List<Usuario> recuperaTodos = usuarioRepository.recuperaTodos();
		
		if (null!=recuperaTodos && !recuperaTodos.isEmpty()) {
			recuperaTodos.forEach(item -> {
				item.setSenha(StringUtils.EMPTY);
			});
			return mapper.converterTodosUsuario(recuperaTodos);
		}
		
		return new ArrayList<UsuarioModel>();
		
	}

}