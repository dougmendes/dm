package br.com.dm.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.dm.domain.Usuario;
import br.com.dm.security.JwtUser;
import br.com.dm.usuario.dao.UsuarioDao;

@Service
public class JwtUserDetailsServiceImpl implements UserDetailsService {

	private UsuarioDao usuarioDao;
	
	public JwtUserDetailsServiceImpl(UsuarioDao usuarioDao) {
		this.usuarioDao=usuarioDao;
	}
	
    @Override
    public UserDetails loadUserByUsername(String cpf) throws UsernameNotFoundException {
    	
    	Usuario usuario = usuarioDao.recuperarUsuario(cpf);
    	
    	return new JwtUser(usuario.getNome(), usuario.getSenha(), usuario.getCpf(), buscarPermissoes(usuario));
    }
    
    public Collection<GrantedAuthority> buscarPermissoes(Usuario usuario) {
		List<GrantedAuthority> permissoes = new ArrayList<>();

		permissoes.add(new SimpleGrantedAuthority(usuario.getRole()));

		return permissoes;
	}
    
}