package br.com.dm.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import br.com.dm.controller.AuthApi;
import br.com.dm.model.TokenModel;
import br.com.dm.service.AuthService;

@RestController
public class AuthApiController implements AuthApi {

	private AuthService authService;
	
	@Autowired
	public AuthApiController(AuthService authService) {
		this.authService = authService;
	}

	@Override
	public ResponseEntity<TokenModel> auth(@RequestHeader(value="Authorization", required=true) String authorization) {
		return new ResponseEntity<>(authService.autenticaUsuario(authorization), HttpStatus.OK);
	}

}