package br.com.dm.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.com.dm.controller.DocumentosApi;
import br.com.dm.service.DocumentoService;

@RestController
public class DocumentosApiController implements DocumentosApi {

	@Autowired
	private DocumentoService documentoService;
	
	public DocumentosApiController(DocumentoService documentoService) {
		this.documentoService=documentoService;
	}
	
	@PreAuthorize("hasAnyAuthority('ATENDIMENTO', 'ADMIN')")
	@Override
	public ResponseEntity<Void> upload(@RequestPart("file") MultipartFile documento, @PathVariable("cpf") String cpf) {
		documentoService.upload(documento, cpf);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
	@PreAuthorize("hasAnyAuthority('ATENDIMENTO', 'ADMIN')")
	@Override
	public ResponseEntity<Resource> download(@PathVariable("cpf") String cpf) {
		return ((ResponseEntity<Resource>)documentoService.download(cpf));
	}
	
}