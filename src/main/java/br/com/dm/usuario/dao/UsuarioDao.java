package br.com.dm.usuario.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import br.com.dm.domain.Usuario;

@Component
public interface UsuarioDao {

	Usuario recuperarUsuario(String cpf);
	
	void cadastrarUsuario(Usuario usuario);

	void alterarUsuario(Usuario usuario);

	void deletarUsuario(String cpf);
	
	List<Usuario> recuperaTodos();

}