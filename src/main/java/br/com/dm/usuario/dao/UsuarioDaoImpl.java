package br.com.dm.usuario.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDeleteExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig.SaveBehavior;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBSaveExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ExpectedAttributeValue;

import br.com.dm.domain.Usuario;
import br.com.dm.exception.UnprocessableException;
import br.com.dm.util.ExceptionUtil;

@Component
public class UsuarioDaoImpl implements UsuarioDao {

	private DynamoDBMapper dynamoDBMapper;
	private static final String ERRO_AO_BUSCAR_USUARIO = "erro ao buscar usuario";
	private static final String ERRO_AO_CADASTRAR_USUARIO = "erro ao cadastrar usuario";
	private static final String ERRO_AO_DELETAR_USUARIO = "erro ao deletar usuario";
	private static final String ERRO_AO_ALTERAR_USUARIO = "erro ao alterar usuario";
	Logger logger = LoggerFactory.getLogger(UsuarioDaoImpl.class);

	@Autowired
	public UsuarioDaoImpl(DynamoDBMapper dynamoDBMapper) {
		this.dynamoDBMapper = dynamoDBMapper;
	}
	
	@Override
	public List<Usuario> recuperaTodos() {
		try {
			return dynamoDBMapper.scan(Usuario.class, new DynamoDBScanExpression());
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new UnprocessableException(ExceptionUtil.erro(ERRO_AO_BUSCAR_USUARIO));
		}
	}

	@Override
	public void cadastrarUsuario(Usuario usuario) {
		try {
			dynamoDBMapper.save(usuario, SaveBehavior.UPDATE_SKIP_NULL_ATTRIBUTES.config());
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new UnprocessableException(ExceptionUtil.erro(ERRO_AO_CADASTRAR_USUARIO));
		}
	}

	public Usuario recuperarUsuario(String cpf) {
		try {
			return dynamoDBMapper.load(Usuario.class, cpf);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new UnprocessableException(ExceptionUtil.erro(ERRO_AO_BUSCAR_USUARIO));
		}
		
	}
	
	@Override
	public void alterarUsuario(Usuario usuario) {
		try {
			DynamoDBSaveExpression saveExpression = new DynamoDBSaveExpression()
					.withExpected(montaExpectedAttributeValue(usuario.getCpf()));
			dynamoDBMapper.save(usuario, saveExpression);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new UnprocessableException(ExceptionUtil.erro(ERRO_AO_DELETAR_USUARIO));
		}
		
	}

	@Override
	public void deletarUsuario(String cpf) {
		try {
			Map<String, ExpectedAttributeValue> expectedAttributeValueMap = new HashMap<>();
			expectedAttributeValueMap.put("cpf", new ExpectedAttributeValue(new AttributeValue().withS(cpf)));
			DynamoDBDeleteExpression deleteExpression = new DynamoDBDeleteExpression()
					.withExpected(montaExpectedAttributeValue(cpf));
			Usuario usuario = Usuario.builder().cpf(cpf).build();
			dynamoDBMapper.delete(usuario, deleteExpression);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new UnprocessableException(ExceptionUtil.erro(ERRO_AO_ALTERAR_USUARIO));
		}
		
	}

	private Map<String, ExpectedAttributeValue> montaExpectedAttributeValue(String valor) {
		Map<String, ExpectedAttributeValue> expectedAttributeValueMap = new HashMap<>();
		expectedAttributeValueMap.put("cpf", new ExpectedAttributeValue(new AttributeValue().withS(valor)));
		return expectedAttributeValueMap;
	}

}