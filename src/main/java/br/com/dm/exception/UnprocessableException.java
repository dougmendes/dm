package br.com.dm.exception;

import br.com.dm.model.ErroModel;
import lombok.Getter;

@Getter
public class UnprocessableException extends RuntimeException {
	
	private static final long serialVersionUID = 3277307522603507849L;
	
	private ErroModel erroModel;   
	
	public UnprocessableException(ErroModel erroModel) {
		this.erroModel=erroModel;
	}
	
}