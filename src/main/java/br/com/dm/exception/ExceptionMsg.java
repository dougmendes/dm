package br.com.dm.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import br.com.dm.model.ErroModel;

@ControllerAdvice
public class ExceptionMsg {

	@ExceptionHandler(UnprocessableException.class)
	public ResponseEntity<ErroModel> handleExecutionRestrictionViolationException(
			UnprocessableException ex) {
		return new ResponseEntity<>(ex.getErroModel(), HttpStatus.UNPROCESSABLE_ENTITY);
	}
	
}